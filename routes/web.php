<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [\App\Http\Controllers\StatisticController::class, 'index'])->name('statistic.index')->middleware('auth');

Route::group(['prefix' => 'auth'], function () {
    Route::get('/login', [\App\Http\Controllers\AuthController::class, 'index'])->name('auth.login.index');
    Route::post('/store', [\App\Http\Controllers\AuthController::class, 'login'])->name('auth.login.store');
    Route::get('/logout',[\App\Http\Controllers\AuthController::class,'logout'])->name('auth.logout');

});
