@extends('layouts.main')



@section('content')

    <div class="container mt-5">
        <form method="post" action="{{route('auth.login.store')}}">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" id="exampleInputEmail1"
                       aria-describedby="emailHelp"
                       placeholder="Enter email">
                <p>
                    @error('email')
                    {{$message}}
                    @enderror
                </p>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                       placeholder="Password">
                <p style="color: red">
                    @error('password')
                    {{$message}}
                    @enderror
                </p>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

        <div class="mt-5">
            <p>Данные для входа в кабинеты:</p>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Email</th>
                    <th scope="col">Password</th>
                    <th scope="col">Role</th>
                    <th scope="col">Team</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->email}}</td>
                        <td>{{ $user->password }}</td>
                        <td>{{ $user->role_name }}</td>
                        <td>{{ $user->team_name }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
