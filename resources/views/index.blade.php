@extends('layouts.main')



@section('content')
    <div class="container mt-5">
        <div class="d-flex justify-content-between">
            <h4>{{\Illuminate\Support\Facades\Auth::user()->role->role_name}}</h4>
            <a href="{{route('auth.logout')}}">Logout</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Team</th>
                <th scope="col">User ID</th>
                <th scope="col">Value</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $item)
                <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->team_name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->value}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
