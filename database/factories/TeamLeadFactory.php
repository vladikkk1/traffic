<?php

namespace Database\Factories;

use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\TeamLead>
 */
class TeamLeadFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
//        return [
//            'user_id' => fake()->randomKey(User::query()->where('role_id', '=', 2)->get()),
//            'team_id' => fake()
//                ->randomKey(
//                    Team::query()
//                        ->join('users','user_id' , '=' , 'teams.user_id')
//                        ->where('users.role_id' , '=' , 2)
//                        ->where()
//                )
//        ];
    }
}
