<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Role;
use App\Models\Statistic;
use App\Models\Team;
use App\Models\TeamLead;
use App\Models\User;
use App\Models\UserPassword;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {


        $roles = [
            [
                'role_name' => 'admin'
            ],
            [
                'role_name' => 'teamLead'
            ],
            [
                'role_name' => 'bayer'
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
        $users = [
            [
                'name' => fake()->name(),
                'email' => 'admin@gmail.com',
                'email_verified_at' => now(),
                'password' => 'admin',
                'role_id' => 1,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => fake()->name(),
                'email' => 'teamLead1@gmail.com',
                'email_verified_at' => now(),
                'password' => 'teamLead1',
                'role_id' => 2,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => fake()->name(),
                'email' => 'teamLead2@gmail.com',
                'email_verified_at' => now(),
                'password' => 'teamLead2',
                'role_id' => 2,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => fake()->name(),
                'email' => 'user1@gmail.com',
                'email_verified_at' => now(),
                'password' => 'user1',
                'role_id' => 3,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => fake()->name(),
                'email' => 'user2@gmail.com',
                'email_verified_at' => now(),
                'password' => 'user2',
                'role_id' => 3,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => fake()->name(),
                'email' => 'user3@gmail.com',
                'email_verified_at' => now(),
                'password' => 'user3',
                'role_id' => 3,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => fake()->name(),
                'email' => 'user4@gmail.com',
                'email_verified_at' => now(),
                'password' => 'user4',
                'role_id' => 3,
                'remember_token' => Str::random(10),
            ],

        ];

        foreach ($users as $user) {
            User::create($user);
            UserPassword::create($user);
        }


        $teamLeads = [
            [
                'user_id' => 2,
                'team_id' => 1
            ],
            [
                'user_id' => 3,
                'team_id' => 2
            ]
        ];

        $teams = [
            [
                'team_name' => 'Team1',
                'user_id' => 4,
                'team_lead_id' => 2
            ],
            [
                'team_name' => 'Team1',
                'user_id' => 5,
                'team_lead_id' => 2
            ],
            [
                'team_name' => 'Team2',
                'user_id' => 6,
                'team_lead_id' => 3
            ],
            [
                'team_name' => 'Team2',
                'user_id' => 7,
                'team_lead_id' => 3
            ],
        ];

        foreach ($teams as $team) {
            Team::create($team);
        }


        foreach ($teamLeads as $teamLead) {
            TeamLead::create($teamLead);
        }


        Statistic::factory(20)->create();


    }
}
