<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Services\Statistic\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatisticController extends Controller
{


    protected $statisticService;

    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    public function index()
    {
        $statisticsFactory = StatisticService::make(Auth::user()->role_id);
        $data = $statisticsFactory->getStatistics();

        return view('index', compact('data'));
    }


}
