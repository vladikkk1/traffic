<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Models\UserPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    public function index()
    {

        $users = UserPassword::select('user_passwords.*', 'roles.role_name', 'teams.team_name')
            ->distinct()
            ->join('roles', 'user_passwords.role_id', '=', 'roles.role_id')
            ->leftJoin('teams', function ($join) {
                $join->on('teams.user_id', '=', 'user_passwords.id')
                    ->orWhere('teams.team_lead_id', '=', 'user_passwords.id');
            })
            ->get();

        return view('auth.login', compact('users'));
    }


    public function login(LoginRequest $loginRequest)
    {
        $data = $loginRequest->validated();
        if (Auth::attempt($data)) {

            $user = Auth::getProvider()->retrieveByCredentials($data);

            Auth::login($user);

            return redirect()->route('statistic.index');
        }
        return back()->withErrors(['password' => 'Wrong password!']);
    }


    public function logout()
    {
        Auth::logout();

        return redirect()->route('auth.login.index');
    }
}
