<?php

namespace App\Services\Statistic;



class StatisticService
{

    public static function make($role)
    {

        switch ($role){
            case 1:
                return new AdminStatistic();
            case 2:
                return new TeamLeadStatistic();
            case 3:
                return new BayerStatistic();
        }
    }


}
