<?php

namespace App\Services\Statistic;

interface StatisticInterface
{

    public function getStatistics();

}
