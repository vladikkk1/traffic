<?php

namespace App\Services\Statistic;

use App\Models\Statistic;
use Illuminate\Support\Facades\Auth;

class TeamLeadStatistic implements StatisticInterface
{

    public function getStatistics()
    {
        return Statistic::query()
            ->join('users' , 'users.id' , '=' , 'statistics.user_id')
            ->join('teams', 'statistics.user_id', '=', 'teams.user_id')
            ->where('teams.team_lead_id', '=', Auth::id())
            ->select('statistics.value','statistics.id', 'statistics.user_id', 'teams.team_name','users.email')
            ->orderByDesc('statistics.id')
            ->get();
    }
}
